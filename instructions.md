# TP API & TypeOrm

## Dog entity

```
name: string;
age: string;
breed: string; (ou un enum pour ceux qui sont à l'aise avec ça)
weight: number;
color: string;
dogId: string (uuid si possible pour le type typeorm, id autogénéré)
owner: User
ownerId: string (id de l'utilisateur propriétaire)
```

## User entity

```
firstName: string;
lastName: string;
userId: string  (uuid si possible pour le type typeorm, id autogénéré)
birthDate: Date;
password: string;
email: string;
```

## Étapes

1. Créer l'entité `Dog`
2. Créer l'entité `User`
3. Créer un CRUD complet pour le `Dog`
   (POST pour la création, PUT pour l'update, GET pour la liste de tous les chiens, GET pour un seul chien avec son Id et DELETE pour la suppression)
4. Créer un CRUD complet pour le `User` (pareil que pour `Dog`)

Temps: 1h30-2h (possible ajustement)
